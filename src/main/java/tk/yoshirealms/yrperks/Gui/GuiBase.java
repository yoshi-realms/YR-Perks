package tk.yoshirealms.yrperks.Gui;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import tk.yoshirealms.yrperks.Yrperks;

/**
 * Created by dragontamerfred on 7-8-17.
 */
public class GuiBase {

    public static Inventory create(String name, int rows) {
        Inventory inv = Bukkit.createInventory(null, rows*9, name);
        for (int i=0;i<=rows*9-1;i++) {
            inv.setItem(i, Yrperks.fillerItem);
        }
        return inv;
    }
}
