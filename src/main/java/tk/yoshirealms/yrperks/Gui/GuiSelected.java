package tk.yoshirealms.yrperks.Gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.yoshirealms.yrperks.Perks.IPerk;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.perk;

import java.util.List;

/**
 * Created by dragontamerfred on 7-8-17.
 */
public class GuiSelected  implements Listener {

    public void open(Player player) {
        Inventory inv = GuiBase.create(Yrperks.guiSelectedName, 5);

        ItemStack doubleJumpPerk = new ItemStack(Material.TRIPWIRE_HOOK);
        ItemStack mainPerks = new ItemStack(Material.TRIPWIRE_HOOK);
        ItemStack itemUsePerk = new ItemStack(Material.TRIPWIRE_HOOK);

        ItemMeta doubleJumpPerkMeta = doubleJumpPerk.getItemMeta();
        ItemMeta mainPerksMeta = mainPerks.getItemMeta();
        ItemMeta itemUsePerkMeta = itemUsePerk.getItemMeta();

        doubleJumpPerkMeta.setDisplayName("Double Jump Perk");
        mainPerksMeta.setDisplayName("Main Perks");
        itemUsePerkMeta.setDisplayName("Item Perk");

        doubleJumpPerk.setItemMeta(doubleJumpPerkMeta);
        mainPerks.setItemMeta(mainPerksMeta);
        itemUsePerk.setItemMeta(itemUsePerkMeta);

        inv.setItem(10, doubleJumpPerk);
        inv.setItem(13, mainPerks);
        inv.setItem(16, itemUsePerk);
        inv.setItem(31, mainPerks);


        if (perk.getSelected(player) != null) {
            List<IPerk> perks = perk.getSelected(player);
            if (perks.get(0) != null) {
                inv.setItem(19, perk.getItem(perks.get(0), player));
            } else {
                inv.setItem(19, Yrperks.noPerkItem);
            }
            if (perks.get(1) != null) {
                inv.setItem(21, perk.getItem(perks.get(1), player));
            } else {
                inv.setItem(21, Yrperks.noPerkItem);
            }
            if (perks.get(2) != null) {
                inv.setItem(22, perk.getItem(perks.get(2), player));
            } else {
                inv.setItem(22, Yrperks.noPerkItem);
            }
            if (perks.get(3) != null) {
                inv.setItem(23, perk.getItem(perks.get(3), player));
            } else {
                inv.setItem(23, Yrperks.noPerkItem);
            }
            if (perks.get(4) != null) {
                inv.setItem(25, perk.getItem(perks.get(4), player));
            } else {
                inv.setItem(25, Yrperks.noPerkItem);
            }
        } else {
            inv.setItem(19, Yrperks.noPerkItem);
            inv.setItem(21, Yrperks.noPerkItem);
            inv.setItem(22, Yrperks.noPerkItem);
            inv.setItem(23, Yrperks.noPerkItem);
            inv.setItem(25, Yrperks.noPerkItem);
        }
        player.openInventory(inv);
    }


    @EventHandler
    public void onPlayerInventoryClick(InventoryClickEvent event) {
        if (event.getWhoClicked().getOpenInventory().getTitle().equals(Yrperks.guiSelectedName)) {
            switch (event.getSlot()) {
                case 19: {
                    new GuiSelecter().open((Player) event.getWhoClicked(), 0);
                    event.setCancelled(true);
                    break;
                }
                case 21: {
                    new GuiSelecter().open((Player) event.getWhoClicked(), 1);
                    event.setCancelled(true);
                    break;
                }
                case 22: {
                    new GuiSelecter().open((Player) event.getWhoClicked(), 2);
                    event.setCancelled(true);
                    break;
                }
                case 23: {
                    new GuiSelecter().open((Player) event.getWhoClicked(), 3);
                    event.setCancelled(true);
                    break;
                }
                case 25: {
                    new GuiSelecter().open((Player) event.getWhoClicked(), 4);
                    event.setCancelled(true);
                    break;
                }
                default: {
                    event.setCancelled(true);
                    break;
                }
            }
        }
    }
}
