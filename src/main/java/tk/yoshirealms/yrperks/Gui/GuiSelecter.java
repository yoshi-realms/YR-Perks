package tk.yoshirealms.yrperks.Gui;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import tk.yoshirealms.yrperks.Perks.IPerk;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.perk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by dragontamerfred on 7-8-17.
 */
public class GuiSelecter implements Listener {

    private static HashMap<Player, Integer> currentSlot = new HashMap<>();

    public void open(Player player, int slot) {
        Inventory inv = GuiBase.create(Yrperks.guiSelecterName, 5);

        List<IPerk> perkList = new ArrayList<>();
        switch (slot) {
            case 0: {
                perkList.addAll(perk.getDoubleJumpPerks());
                break;
            }
            case 1: {
                perkList.addAll(perk.getDeathPerks());
                perkList.addAll(perk.getKillPerks());
                break;
            }
            case 2: {
                perkList.addAll(perk.getDeathPerks());
                perkList.addAll(perk.getKillPerks());
                break;
            }
            case 3: {
                perkList.addAll(perk.getDeathPerks());
                perkList.addAll(perk.getKillPerks());
                break;
            }
            case 4: {
                perkList.addAll(perk.getUsePerks());
                break;
            }
        }

        int i = 10;
        for (IPerk iPerk : perkList) {
            inv.setItem(i, perk.getItem(iPerk, player));
            if (i==16)
                i=18;
            if (i==25)
                i=27;
            i++;
        }
        currentSlot.put(player, slot);
        player.closeInventory();
        player.openInventory(inv);
    }


    @EventHandler
    public void onPlayerInventoryClick(InventoryClickEvent event) {
        if (event.getWhoClicked().getOpenInventory().getTitle().equals(Yrperks.guiSelecterName)) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equals(Yrperks.fillerItem.getItemMeta().getDisplayName())) {
                event.setCancelled(true);
            } else if (currentSlot.containsKey(event.getWhoClicked())) {
                perk.selectPerk((Player) event.getWhoClicked(), currentSlot.get(event.getWhoClicked()), perk.getPerk(event.getCurrentItem().getItemMeta().getDisplayName()));
                event.setCancelled(true);
            } else {
                event.setCancelled(true);
            }
        }
    }
}
