package tk.yoshirealms.yrperks;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.yoshirealms.yrperks.Perks.IPerk;
import tk.yoshirealms.yrperks.Perks.perkNoPerk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static tk.yoshirealms.yrperks.Yrperks.save;

/**
 * Created by dragontamerfred on 15-6-17.
 */
public class perk {
    private static HashMap<String, IPerk> perks = new HashMap<>();
    private static List<IPerk> deathPerks = new ArrayList<>();
    private static List<IPerk> doubleJumpPerks = new ArrayList<>();
    private static List<IPerk> usePerks = new ArrayList<>();
    private static List<IPerk> killPerks = new ArrayList<>();
    public static HashMap<UUID, List<IPerk>> selected = new HashMap<>();
    public static HashMap<UUID, List<IPerk>> unlocked = new HashMap<>();

    public static void registerPerk(IPerk iPerk) {
        if (!perks.containsKey(iPerk.getName())) {
            System.out.println(Yrperks.prefix + "registerd perk: " + iPerk.getName());
            perks.put(iPerk.getName(), iPerk);
            switch (iPerk.getType()) {
                case DEATH: deathPerks.add(iPerk); break;
                case DOUBLEJUMP: doubleJumpPerks.add(iPerk); break;
                case USE: usePerks.add(iPerk); break;
                case KILL: killPerks.add(iPerk); break;
            }
        }
    }

    public static boolean isRegistered(String name) {
        return perks.containsKey(name);
    }

    public static List<IPerk> getDeathPerks() {
        return deathPerks;
    }

    public static List<IPerk> getDoubleJumpPerks() {
        return doubleJumpPerks;
    }

    public static List<IPerk> getUsePerks() {
        return usePerks;
    }

    public static List<IPerk> getKillPerks() {
        return killPerks;
    }

    public static IPerk getPerk(String name) {
        return perks.getOrDefault(name, null);
    }

    public static void runKillPerks(Player player) {
        if (selected.containsKey(player.getUniqueId())) {
            for (IPerk perk : selected.get(player.getUniqueId())) {
                if (perk.getType() == enumPerkType.KILL) {
                    perk.execute(player);
                }
            }
        }
    }

    public static void runDeathPerks(Player player) {
        if (selected.containsKey(player.getUniqueId())) {
            for (IPerk perk : selected.get(player.getUniqueId())) {
                if (perk.getType() == enumPerkType.DEATH) {
                    perk.execute(player);
                }
            }
        }
    }

    public static void runUsePerks(Player player) {
        if (selected.containsKey(player.getUniqueId())) {
            for (IPerk perk : selected.get(player.getUniqueId())) {
                if (perk.getType() == enumPerkType.USE) {
                    perk.execute(player);
                }
            }
        }
    }

    public static void runDoubleJumpPerks(Player player) {
        if (selected.containsKey(player.getUniqueId())) {
            for (IPerk perk : selected.get(player.getUniqueId())) {
                if (perk.getType() == enumPerkType.DOUBLEJUMP) {
                    perk.execute(player);
                }
            }
        }
    }

    public static List<IPerk> getSelected(Player player) {
        return selected.getOrDefault(player.getUniqueId(), null);
    }

    public static void selectPerk(Player player, int slot, IPerk perk) {
        if (unlocked.containsKey(player.getUniqueId())) {
            if (unlocked.get(player.getUniqueId()).contains(perk)) {
                internalSelectPerk(player, slot, perk);
            } else {
                if (buyPerk(player, perk)) {
                    internalSelectPerk(player, slot, perk);
                }
            }
        } else {
            if (buyPerk(player, perk)) {
                internalSelectPerk(player, slot, perk);
            }
        }
        save();
    }

    private static void internalSelectPerk(Player player, int slot, IPerk perk) {
        if (selected.containsKey(player.getUniqueId())) {
            List<IPerk> perks = selected.get(player.getUniqueId());
            perks.set(slot, perk);
            selected.put(player.getUniqueId(), perks);
        } else {
            List<IPerk> perks = new ArrayList<>();
            perks.add(new perkNoPerk());
            perks.add(new perkNoPerk());
            perks.add(new perkNoPerk());
            perks.add(new perkNoPerk());
            perks.add(new perkNoPerk());
            perks.set(slot, perk);
            selected.put(player.getUniqueId(), perks);
        }
    }

    private static boolean buyPerk(Player player, IPerk perk) {
        if (Yrperks.economy.getBalance(player) >= perk.getCost()) {
            if (unlocked.containsKey(player.getUniqueId())) {
                List<IPerk> perks = unlocked.get(player.getUniqueId());
                perks.add(perk);
                unlocked.put(player.getUniqueId(), perks);
                return true;
            } else {
                List<IPerk> perks = new ArrayList<>();
                perks.add(perk);
                unlocked.put(player.getUniqueId(), perks);
                return true;
            }
        }
        return false;
    }

    public static ItemStack getItem(IPerk perk, Player player) {
        ItemStack itemStack = new ItemStack(perk.getItem(), 1, perk.getItemData());
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(perk.getName());
        List<String> list = new ArrayList<>();
        list.addAll(perk.getDesc());
        list.add("Cost: " + perk.getCost());
        if (unlocked.containsKey(player.getUniqueId())) {
            if (unlocked.get(player.getUniqueId()).contains(perk))
                list.add("Unlocked");
            list.add("Locked");
        } else {
            list.add("Locked");
        }
        itemMeta.setLore(list);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}
