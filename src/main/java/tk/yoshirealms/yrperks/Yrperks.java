package tk.yoshirealms.yrperks;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.Configuration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import tk.yoshirealms.yrperks.Classes.classWarrior;
import tk.yoshirealms.yrperks.Commands.commandPerks;
import tk.yoshirealms.yrperks.Gui.GuiSelected;
import tk.yoshirealms.yrperks.Gui.GuiSelecter;
import tk.yoshirealms.yrperks.Listeners.DeathListener;
import tk.yoshirealms.yrperks.Listeners.DoubleJumpListener;
import tk.yoshirealms.yrperks.Listeners.ItemUseListener;
import tk.yoshirealms.yrperks.Listeners.KillListener;
import tk.yoshirealms.yrperks.Perks.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class Yrperks extends JavaPlugin {

    public static String prefix = "[YR-Perks] ";
    public static ItemStack skillItem = new ItemStack(Material.PAPER), fillerItem = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 8),
            noPerkItem = new ItemStack(Material.BARRIER);
    public static Yrperks plugin;
    public static Configuration config;
    public static Economy economy;
    public static String guiSelectedName = "Selected Perks";
    public static String guiSelecterName = "Select Perk";
    public static String guiClassSelecterName = "Select Class";
    public static String abilityOnCooldown = "This ability is still on cooldown.";

    @Override
    public void onEnable() {
        plugin = this;
        config = getConfig();
        load();
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(new DeathListener(), this);
        getServer().getPluginManager().registerEvents(new DoubleJumpListener(), this);
        getServer().getPluginManager().registerEvents(new ItemUseListener(), this);
        getServer().getPluginManager().registerEvents(new KillListener(), this);
        getServer().getPluginManager().registerEvents(new GuiSelected(), this);
        getServer().getPluginManager().registerEvents(new GuiSelecter(), this);
        getServer().getPluginCommand("perks").setExecutor(new commandPerks());
        setupEconomy();

        ItemMeta fillerMeta = fillerItem.getItemMeta();
        ItemMeta noPerkItemMeta = noPerkItem.getItemMeta();
        fillerMeta.setDisplayName(" ");
        noPerkItemMeta.setDisplayName("No perk");
        fillerItem.setItemMeta(fillerMeta);
        noPerkItem.setItemMeta(noPerkItemMeta);

        //Use perks
        perk.registerPerk(new perkHeal());
        perk.registerPerk(new perkGiveArrows());
        perk.registerPerk(new perkTank());
        perk.registerPerk(new perkNinjaDash());
        //Doublejump perks
        perk.registerPerk(new perkGroundSlam());
        perk.registerPerk(new perkLeaping());
        perk.registerPerk(new perkWeakness());
        perk.registerPerk(new perkNinjaJump());
        //Death perks
        perk.registerPerk(new perkSummonZombie());
        perk.registerPerk(new perkSummonSkeleton());
        perk.registerPerk(new perkPoison());
        perk.registerPerk(new perkSlowness());
        //Kill perks
        perk.registerPerk(new perkStrength());
        perk.registerPerk(new perkRegen());
        perk.registerPerk(new perkPowerUp());
        perk.registerPerk(new perkResistance());
        perk.registerPerk(new perkArmorUp());
        perk.registerPerk(new perkExtraHealth());
        perk.registerPerk(new perkInvis());
        perk.registerPerk(new perkSpeed());


        //register classes
        classs.registerClass(new classWarrior());
    }

    //todo make classes

    @Override
    public void onDisable() {
        save();
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

    public static void save() {
        List<String> storeList1 = new ArrayList<>();
        for (UUID uuid : perk.selected.keySet()) {
            String str = uuid.toString() + ":";
            boolean first = true;
            for (IPerk perk : perk.selected.get(uuid)) {
                if (first) {
                    str += perk.getName();
                    first = false;
                } else {
                    str += ";" + perk.getName();
                }
            }
            Bukkit.broadcastMessage(str);
            storeList1.add(str);
        }
        config.set("selected", storeList1);

        List<String> storeList2 = new ArrayList<>();
        for (UUID uuid : perk.unlocked.keySet()) {
            String str = uuid.toString() + ":";
            boolean first = true;
            for (IPerk perk : perk.unlocked.get(uuid)) {
                if (first) {
                    str += perk.getName();
                    first = false;
                } else {
                    str += ";" + perk.getName();
                }
            }
            Bukkit.broadcastMessage(str);
            storeList2.add(str);
        }
        config.set("unlocked", storeList2);
    }

    public static void load() {
        List<String> storeList1 = config.getStringList("selected");
        for (String str : storeList1) {
            String[] strings1 = str.split(":");
            String[] strings2 = strings1[1].split(";");
            List<IPerk> perks = new ArrayList<>();
            perks.add(perk.getPerk(strings2[0]));
            perks.add(perk.getPerk(strings2[1]));
            perks.add(perk.getPerk(strings2[2]));
            perks.add(perk.getPerk(strings2[3]));
            perks.add(perk.getPerk(strings2[4]));
            perk.selected.put(UUID.fromString(strings1[0]), perks);
        }
        List<String> storeList2 = config.getStringList("unlocked");
        for (String str : storeList2) {
            String[] strings1 = str.split(":");
            String[] strings2 = strings1[1].split(";");
            List<IPerk> perks = new ArrayList<>();
            for (int i = 0; i < strings2.length; i++)
                perks.add(perk.getPerk(strings2[i]));
            perk.unlocked.put(UUID.fromString(strings1[0]), perks);
        }
    }
}
