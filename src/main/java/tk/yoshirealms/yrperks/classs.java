package tk.yoshirealms.yrperks;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.yoshirealms.yrperks.Classes.IClass;

import java.util.*;

/**
 * Created by dragontamerfred on 10-8-17.
 */
public class classs {

    private static HashMap<String, IClass> classes = new HashMap<>();
    private static HashMap<UUID, IClass> selected = new HashMap<>();
    private static HashMap<UUID, List<IClass>> unlocked = new HashMap<>();

    public static void registerClass(IClass classs) {
        classes.put(classs.getName(), classs);
    }

    public static void selectClass(Player player, IClass classs) {
        if (unlocked.containsKey(player.getUniqueId())) {
            if (unlocked.get(player.getUniqueId()).contains(classs)) {
                selected.put(player.getUniqueId(), classs);
            } else {
                unlockAndSelect(player, classs);
            }
        } else {
            unlockAndSelect(player, classs);
        }
    }

    private static void unlockAndSelect(Player player, IClass classs) {
        if (Yrperks.economy.getBalance(player) >= classs.getCost()) {
            Yrperks.economy.withdrawPlayer(player, classs.getCost());
            List<IClass> unlockedClasses = unlocked.getOrDefault(player.getUniqueId(), new ArrayList<>());
            unlockedClasses.add(classs);
            unlocked.put(player.getUniqueId(), unlockedClasses);
            selected.put(player.getUniqueId(), classs);
        }
    }

    public static List<IClass> getClasses() {
        List<IClass> IClasses = new ArrayList<>();
        IClasses.addAll(classes.values());
        return IClasses;
    }

    public static ItemStack getItem(IClass iClass) {
        ItemStack itemStack = new ItemStack(iClass.getItem(), 1, iClass.getItemData());
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(iClass.getName());
        List<String> list = new ArrayList<>();
        list.add(iClass.getDesc());
        list.add("Cost: " + iClass.getCost());
        itemMeta.setLore(list);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}
