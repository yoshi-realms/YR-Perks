package tk.yoshirealms.yrperks.Commands;

import com.sk89q.util.StringUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrperks.Gui.GuiSelected;
import tk.yoshirealms.yrperks.perk;

/**
 * Created by dragontamerfred on 12-8-17.
 */
public class commandPerks implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player) {
            if (args.length >= 1 && sender.hasPermission("YR-Perks.perks.test")) {
                if (perk.isRegistered(StringUtil.joinString(args, " "))) {
                    perk.getPerk(StringUtil.joinString(args, " ")).execute((Player) sender);
                }
            } else {
                Player player = (Player) sender;
                new GuiSelected().open(player);
            }
        }
        return true;
    }
}
