package tk.yoshirealms.yrperks.Classes;

import org.bukkit.Material;
import tk.yoshirealms.yrperks.Perks.IPerk;

import java.util.List;

/**
 * Created by dragontamerfred on 10-8-17.
 */
public interface IClass {

    String getName();
    String getDesc();
    List<IPerk> getPerks();
    int getCost();
    int getMinLevel();
    Material getItem();
    Short getItemData();
}
