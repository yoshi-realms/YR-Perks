package tk.yoshirealms.yrperks.Classes;

import org.bukkit.Material;
import tk.yoshirealms.yrperks.Perks.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 10-8-17.
 */
public class classWarrior implements IClass {

    @Override
    public String getName() {
        return "Warrior";
    }

    @Override
    public String getDesc() {
        return "";
    }

    @Override
    public List<IPerk> getPerks() {
        List<IPerk> perks = new ArrayList<>();
        perks.add(new perkRegen());
        perks.add(new perkStrength());
        perks.add(new perkSummonZombie());
        perks.add(new perkGroundSlam());
        perks.add(new perkHeal());
        return perks;
    }

    @Override
    public int getCost() {
        return 3000;
    }

    @Override
    public int getMinLevel() {
        return 5;
    }

    @Override
    public Material getItem() {
        return Material.DIAMOND_SWORD;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
