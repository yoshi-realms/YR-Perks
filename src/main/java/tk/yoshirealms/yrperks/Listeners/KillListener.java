package tk.yoshirealms.yrperks.Listeners;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import tk.yoshirealms.yrperks.perk;

import java.util.HashMap;

/**
 * Created by dragontamerfred on 6-8-17.
 */
public class KillListener implements Listener {

    private static HashMap<Player, Player> lastDamage = new HashMap<>();

    @EventHandler
    public void onPlayerKillEvent(PlayerDeathEvent event) {
        Player target = event.getEntity();
        if (!WGBukkit.getRegionManager(target.getWorld()).getApplicableRegions(target.getLocation()).allows(DefaultFlag.PVP))
            return;
        if (lastDamage.containsKey(target)) {
            lastDamage.get(target).sendMessage("You killed " + target.getName());
            perk.runKillPerks(lastDamage.get(target));
        }
    }

    @EventHandler
    public void onPlayerDamageEvent(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player target = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();
            lastDamage.put(target, damager);
        }
    }
}
