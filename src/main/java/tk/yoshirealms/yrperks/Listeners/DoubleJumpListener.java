package tk.yoshirealms.yrperks.Listeners;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.perk;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 6-8-17.
 */
public class DoubleJumpListener implements Listener {

    List<Player> cooldown = new ArrayList<>();

    @EventHandler
    public void onPlayerDoubleJumpEvent(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.CREATIVE)
            return;

        event.setCancelled(true);
        player.setAllowFlight(false);
        player.setFlying(false);

        if (!WGBukkit.getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation()).allows(DefaultFlag.PVP))
            return;
        if (cooldown.contains(player)) {
            player.sendMessage(Yrperks.prefix + Yrperks.abilityOnCooldown);
            return;
        }

        perk.runDoubleJumpPerks(player);
        cooldown.add(player);

        Bukkit.getScheduler().runTaskLater(Yrperks.plugin, () -> {
            player.setAllowFlight(true);
            cooldown.remove(player);
        }, 10*20);
    }

    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent event) {
        if (cooldown.contains(event.getPlayer()))
            return;
        event.getPlayer().setAllowFlight(true);
    }
}
