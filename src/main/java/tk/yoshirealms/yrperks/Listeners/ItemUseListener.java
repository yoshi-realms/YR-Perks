package tk.yoshirealms.yrperks.Listeners;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.perk;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 6-8-17.
 */

public class ItemUseListener implements Listener {

    private List<Player> cooldown = new ArrayList<>();

    @EventHandler
    public void onItemUseEvent(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            Player player = event.getPlayer();
            if (!WGBukkit.getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation()).allows(DefaultFlag.PVP))
                return;
            if (cooldown.contains(player)) {
                player.sendMessage(Yrperks.prefix + Yrperks.abilityOnCooldown);
            } else if (player.getInventory().getItemInHand() == Yrperks.skillItem) {
                cooldown.add(player);
                perk.runUsePerks(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(Yrperks.plugin, () -> cooldown.remove(player), 20*20);
            }
        }
    }
}
