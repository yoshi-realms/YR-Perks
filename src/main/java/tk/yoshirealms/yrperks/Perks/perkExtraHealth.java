package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkExtraHealth implements IPerk {

    @Override
    public void execute(Player player) {
        player.setMaxHealth(player.getMaxHealth() + 2.0);
    }

    @Override
    public String getName() {
        return "Extra health";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Everytime you kill an enemy you receive a bonus hearth");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.KILL;
    }

    @Override
    public Material getItem() {
        return Material.GOLDEN_APPLE;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
