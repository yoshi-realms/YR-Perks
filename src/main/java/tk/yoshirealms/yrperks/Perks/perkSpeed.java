package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkSpeed implements IPerk {
    @Override
    public void execute(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3*20, 0));
    }

    @Override
    public String getName() {
        return "Speed";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("After killing an enemy you recieve");
        desc.add("a speed boost to run away or continue the fight!");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.KILL;
    }

    @Override
    public Material getItem() {
        return Material.POTION;
    }

    @Override
    public Short getItemData() {
        return 8258;
    }
}
