package tk.yoshirealms.yrperks.Perks;

import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.Item;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkSummonSkeleton implements IPerk {
    @Override
    public void execute(Player player) {
        EntityLiving skeleton = ((CraftLivingEntity) player.getWorld().spawnEntity(player.getLocation(), EntityType.ZOMBIE)).getHandle();
        skeleton.setCustomName(player.getName() + "'s skeleton");
        skeleton.setEquipment(0, new net.minecraft.server.v1_8_R3.ItemStack(Item.getById(Material.BOW.getId())));
        skeleton.setEquipment(2, new net.minecraft.server.v1_8_R3.ItemStack(Item.getById(Material.CHAINMAIL_CHESTPLATE.getId())));
        skeleton.setEquipment(4, new net.minecraft.server.v1_8_R3.ItemStack(Item.getById(Material.CHAINMAIL_BOOTS.getId())));
    }

    @Override
    public String getName() {
        return "Summon skeleton";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Even though you might’ve died");
        desc.add("your skeleton isn’t done fighting yet!");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.DEATH;
    }

    @Override
    public Material getItem() {
        return Material.SKULL_ITEM;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
