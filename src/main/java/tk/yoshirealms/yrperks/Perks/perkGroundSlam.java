package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.enumPerkType;

import javax.security.auth.login.FailedLoginException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 7-8-17.
 */
public class perkGroundSlam implements IPerk {

    @Override
    public void execute(Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Yrperks.plugin, () -> player.setVelocity(new Vector(0.0, 0.25, 0.0)),1L);
        player.playSound(player.getLocation(), Sound.ANVIL_LAND, Float.parseFloat("1"), Float.parseFloat("0"));
        int d2 = 3 * 3;
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            if (p.getWorld() == player.getWorld() && p.getLocation().distanceSquared(player.getLocation()) <= d2) {
                p.setVelocity(p.getVelocity().add(new Vector(player.getLocation().toVector().subtract(p.getLocation().toVector()).normalize().multiply(-1.25).getX(),
                                                             player.getLocation().toVector().subtract(p.getLocation().toVector()).normalize().multiply(1.15).getY(),
                                                             player.getLocation().toVector().subtract(p.getLocation().toVector()).normalize().multiply(-1.25).getZ())));
                p.playSound(player.getLocation(), Sound.ANVIL_LAND, Float.parseFloat("1"), Float.parseFloat("0"));
            }
        }
    }

    @Override
    public String getName() {
        return "Ground Slam";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Make a jump and land with a destructing force");
        desc.add("knocking back your enemies!");
        return desc;
    }

    @Override
    public int getCost() {
        return 2000;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.DOUBLEJUMP;
    }

    @Override
    public Material getItem() {
        return Material.SLIME_BLOCK;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
