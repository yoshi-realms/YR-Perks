package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkArmorUp implements IPerk {
    @Override
    public void execute(Player player) {
        Random rand = new Random(System.nanoTime());
        if (rand.nextInt(4) == 0) {
            int armorSlot = rand.nextInt(4);
            ItemStack stack  = player.getEquipment().getArmorContents()[armorSlot];
            switch (stack.getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL)) {
                case 1: {
                    stack.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
                }
                case 2: {
                    stack.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
                }
                case 3: {
                    stack.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
                }
                default: {
                    stack.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
                }
            }
            switch (armorSlot) {
                case 0: player.getEquipment().setHelmet(stack);
                case 1: player.getEquipment().setChestplate(stack);
                case 2: player.getEquipment().setLeggings(stack);
                case 3: player.getEquipment().setBoots(stack);
            }
        }
    }

    @Override
    public String getName() {
        return "Armor up";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Your armor becomes stronger");
        desc.add("as you beat more people while using it!");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.KILL;
    }

    @Override
    public Material getItem() {
        return Material.ENCHANTED_BOOK;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
