package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 7-8-17.
 */
public class perkStrength implements IPerk {
    @Override
    public void execute(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 2*20, 0, false, false));
    }

    @Override
    public String getName() {
        return "Strength";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("The feeling of beating someone");
        desc.add("only makes you feel stronger.");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.KILL;
    }

    @Override
    public Material getItem() {
        return Material.BLAZE_POWDER;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
