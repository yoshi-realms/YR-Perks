package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkNinjaJump implements IPerk {
    @Override
    public void execute(Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Yrperks.plugin, () -> player.setVelocity(player.getVelocity().add(new Vector(player.getLocation().getDirection().multiply(1.0).getX(),
                player.getLocation().getDirection().multiply(1.25).getY(), player.getLocation().getDirection().multiply(1.0).getZ()))),1L);

    }

    @Override
    public String getName() {
        return "Ninja double jump";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Make a jump like only the most");
        desc.add("elite ninjas can to jump on top of buildings");
        return desc;
    }

    @Override
    public int getCost() {
        return 2000;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.DOUBLEJUMP;
    }

    @Override
    public Material getItem() {
        return Material.POTION;
    }

    @Override
    public Short getItemData() {
        return 8203;
    }
}
