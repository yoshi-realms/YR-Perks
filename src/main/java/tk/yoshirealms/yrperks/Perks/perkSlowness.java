package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkSlowness implements IPerk {
    @Override
    public void execute(Player player) {
        int d2 = 4 * 4;
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            if (p.getWorld() == player.getWorld() && p.getLocation().distanceSquared(player.getLocation()) <= d2) {
                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 4*20, 0));
            }
        }
    }

    @Override
    public String getName() {
        return "Slowness cloud";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("After your death your ghost grabs the");
        desc.add("opponents feet, slowing them...");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.DEATH;
    }

    @Override
    public Material getItem() {
        return Material.POTION;
    }

    @Override
    public Short getItemData() {
        return 16426;
    }
}
