package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkGiveArrows implements IPerk {

    @Override
    public void execute(Player player) {
        player.getInventory().addItem(new ItemStack(Material.ARROW, 8));
    }

    @Override
    public String getName() {
        return "Spawn arrow";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("The godess Artemis will give you a few arrows");
        desc.add("everytime you use this item so you can continue");
        desc.add(" fighting in honor of the bow!");
        return desc;
    }

    @Override
    public int getCost() {
        return 2500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.USE;
    }

    @Override
    public Material getItem() {
        return Material.ARROW;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
