package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkTank implements IPerk {
    @Override
    public void execute(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 15*20, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15*20, 2));
    }

    @Override
    public String getName() {
        return "Tank";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("You receive heavy armor for 15 seconds");
        desc.add("which greatly improves your defense");
        desc.add("but you will be slowed down because of it's weight");
        return desc;
    }

    @Override
    public int getCost() {
        return 2500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.USE;
    }

    @Override
    public Material getItem() {
        return Material.DIAMOND_CHESTPLATE;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
