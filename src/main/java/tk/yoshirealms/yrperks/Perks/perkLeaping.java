package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkLeaping implements IPerk {
    @Override
    public void execute(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 6*20, 2));
    }

    @Override
    public String getName() {
        return "Archer leap";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("After double jumping you’ll receive");
        desc.add("a massive jump boost for a few seconds!");
        return desc;
    }

    @Override
    public int getCost() {
        return 2000;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.DOUBLEJUMP;
    }

    @Override
    public Material getItem() {
        return Material.POTION;
    }

    @Override
    public Short getItemData() {
        return 8203;
    }
}
