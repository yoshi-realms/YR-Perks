package tk.yoshirealms.yrperks.Perks;

import net.minecraft.server.v1_8_R3.EntityZombie;
import net.minecraft.server.v1_8_R3.Item;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 7-8-17.
 */
public class perkSummonZombie implements IPerk {

    @Override
    public void execute(Player player) {
        EntityZombie zombie = ((EntityZombie) player.getWorld().spawnEntity(player.getLocation(), EntityType.ZOMBIE));
        zombie.setBaby(true);
        zombie.setCustomName(player.getName() + "'s mini warrior");
        zombie.setEquipment(0, new net.minecraft.server.v1_8_R3.ItemStack(Item.getById(Material.IRON_SWORD.getId())));
        zombie.setEquipment(2, new net.minecraft.server.v1_8_R3.ItemStack(Item.getById(Material.CHAINMAIL_CHESTPLATE.getId())));
        zombie.setEquipment(4, new net.minecraft.server.v1_8_R3.ItemStack(Item.getById(Material.CHAINMAIL_BOOTS.getId())));
    }

    @Override
    public String getName() {
        return "Summon mini warrior.";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("After your death a mini warrior appears");
        desc.add("to get revenge on the foes that killed you!");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.DEATH;
    }

    @Override
    public Material getItem() {
        return Material.SKULL_ITEM;
    }

    @Override
    public Short getItemData() {
        return 2;
    }
}
