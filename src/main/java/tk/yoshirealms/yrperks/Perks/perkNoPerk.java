package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 20-8-17.
 */
public class perkNoPerk implements IPerk {
    @Override
    public void execute(Player player) {

    }

    @Override
    public String getName() {
        return "No perk selected";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Click here to select a perk");
        return desc;
    }

    @Override
    public int getCost() {
        return 0;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.NOPERK;
    }

    @Override
    public Material getItem() {
        return Material.BARRIER;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
