package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkTNTCannon implements IPerk {
    @Override
    public void execute(Player player) {
        TNTPrimed tnt = (TNTPrimed) player.getWorld().spawnEntity(player.getLocation().add(0, 1, 0), EntityType.PRIMED_TNT);
        tnt.setFuseTicks(45);
        tnt.setVelocity(player.getLocation().getDirection().multiply(2));
        Bukkit.getScheduler().runTaskLater(Yrperks.plugin, () -> {
            tnt.getWorld().createExplosion(tnt.getLocation(), 5, false);
            tnt.remove();
        }, 40);
    }

    @Override
    public String getName() {
        return "TNT handcannon";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Shoot a block of TNT");
        desc.add("in the direction you are looking");
        return desc;
    }

    @Override
    public int getCost() {
        return 2000;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.USE;
    }

    @Override
    public Material getItem() {
        return Material.GOLD_HOE;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
