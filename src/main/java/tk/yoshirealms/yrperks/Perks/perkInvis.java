package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkInvis implements IPerk {
    @Override
    public void execute(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 3*20, 0));
        ItemStack[] armor = player.getEquipment().getArmorContents();
        player.getEquipment().setHelmet(new ItemStack(Material.AIR));
        player.getEquipment().setChestplate(new ItemStack(Material.AIR));
        player.getEquipment().setLeggings(new ItemStack(Material.AIR));
        player.getEquipment().setBoots(new ItemStack(Material.AIR));

        Bukkit.getScheduler().runTaskLater(Yrperks.plugin, () -> {
            if (player.getStatistic(Statistic.TIME_SINCE_DEATH) > 3*20) {
                player.getEquipment().setArmorContents(armor);
            }
        }, 3*20);
    }

    @Override
    public String getName() {
        return "Invisibility";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("After getting a kill you will disappear");
        desc.add("from the battlefield for a few seconds,");
        desc.add("giving you a chance to reposition.");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.KILL;
    }

    @Override
    public Material getItem() {
        return Material.POTION;
    }

    @Override
    public Short getItemData() {
        return 8270;
    }
}
