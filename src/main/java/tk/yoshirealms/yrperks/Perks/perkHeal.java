package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 10-8-17.
 */
public class perkHeal implements IPerk {
    @Override
    public void execute(Player player) {
        player.setHealth(player.getHealth() + 4.0);
    }

    @Override
    public String getName() {
        return "Heal";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Low on hearts? No problem!");
        desc.add("This item with the power of the gods");
        desc.add("has a powerful health buff!");
        return desc;
    }

    @Override
    public int getCost() {
        return 2500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.USE;
    }

    @Override
    public Material getItem() {
        return Material.SPECKLED_MELON;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
