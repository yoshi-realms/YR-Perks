package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import tk.yoshirealms.yrperks.Yrperks;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkNinjaDash implements IPerk {
    @Override
    public void execute(Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Yrperks.plugin, () ->
                player.setVelocity(player.getVelocity().add(player.getLocation().getDirection().multiply(4.0))),1L);
    }

    @Override
    public String getName() {
        return "Ninja dash";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Surprise your enemies by dashing forward out of nowhere!");
        return desc;
    }

    @Override
    public int getCost() {
        return 2500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.USE;
    }

    @Override
    public Material getItem() {
        return Material.FEATHER;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
