package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by dragontamerfred on 21-8-17.
 */
public class perkPowerUp implements IPerk {
    @Override
    public void execute(Player player) {
        Random rand = new Random(System.nanoTime());
        if (rand.nextInt(4) == 0) {
            for (int i = 0; i <= player.getInventory().getSize(); i++) {
                if (player.getInventory().getItem(i).getType() == Material.BOW) {
                    switch (player.getInventory().getItem(i).getEnchantmentLevel(Enchantment.ARROW_DAMAGE)) {
                        case 1: {
                            ItemStack stack = player.getInventory().getItem(i);
                            stack.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
                            player.getInventory().setItem(i, stack);
                        }
                        case 2: {
                            ItemStack stack = player.getInventory().getItem(i);
                            stack.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
                            player.getInventory().setItem(i, stack);
                        }
                        case 3: {
                            ItemStack stack = player.getInventory().getItem(i);
                            stack.addEnchantment(Enchantment.ARROW_DAMAGE, 4);
                            player.getInventory().setItem(i, stack);
                        }
                        case 4: {
                            ItemStack stack = player.getInventory().getItem(i);
                            stack.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
                            player.getInventory().setItem(i, stack);
                        }
                        default: {
                            ItemStack stack = player.getInventory().getItem(i);
                            stack.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
                            player.getInventory().setItem(i, stack);
                        }
                    }
                    break;
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Power up";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("As you beat people you become more");
        desc.add("experienced with your bow");
        desc.add("which upgrades your power level.");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.KILL;
    }

    @Override
    public Material getItem() {
        return Material.ENCHANTED_BOOK;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
