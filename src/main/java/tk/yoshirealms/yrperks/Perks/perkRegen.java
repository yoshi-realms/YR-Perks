package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 6-8-17.
 */
public class perkRegen implements IPerk {
    @Override
    public void execute(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 2 * 20, 2, false, false));
    }

    @Override
    public String getName() {
        return "Regen";
    }

    @Override
    public List<String> getDesc() {
        List<String> desc = new ArrayList<>();
        desc.add("Killing an enemy magically heals your wounds");
        return desc;
    }

    @Override
    public int getCost() {
        return 1500;
    }

    @Override
    public enumPerkType getType() {
        return enumPerkType.KILL;
    }

    @Override
    public Material getItem() {
        return Material.GHAST_TEAR;
    }

    @Override
    public Short getItemData() {
        return 0;
    }
}
