package tk.yoshirealms.yrperks.Perks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tk.yoshirealms.yrperks.enumPerkType;

import java.util.List;

/**
 * Created by dragontamerfred on 6-8-17.
 */
public interface IPerk {


    void execute(Player player);

    String getName();
    List<String> getDesc();
    int getCost();
    enumPerkType getType();
    Material getItem();
    Short getItemData();
}
