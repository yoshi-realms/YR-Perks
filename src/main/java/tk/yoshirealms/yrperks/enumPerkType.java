package tk.yoshirealms.yrperks;

/**
 * Created by dragontamerfred on 6-8-17.
 */
public enum enumPerkType {
    KILL,
    DEATH,
    USE,
    DOUBLEJUMP,
    NOPERK
}
